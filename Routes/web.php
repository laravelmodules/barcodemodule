<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Modules\BarCode\Http\Controllers\Api\BarCodeController;
use Modules\BarCode\Lib\BarCodeService;

Route::get('barcode', 'Api\BarCodeController');
Route::post('get-barcode', 'Api\PrepareBarCodeController');
