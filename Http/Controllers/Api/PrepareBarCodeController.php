<?php

namespace Modules\BarCode\Http\Controllers\Api;

use App\Models\ChatBotTemplate;
use App\Models\StoryBot;
use App\Models\Subscriber;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class PrepareBarCodeController extends Controller
{
    private $subscriber;
    public function __invoke(Request $request)
    {
        $template = ChatBotTemplate::where('bot_id', $request->bot_id)
            ->where('name', 'barcode')->firstOrFail();

        $this->subscriber = Subscriber::with([
        'options' => function ($query) {
            $query->where('parameter', 'barcode');
        },
    ])
        ->find($request->user_id);

        /** @var \Illuminate\Support\Collection $options */
        $options = optional($this->subscriber->options)->first();

        if (! $options) {
            return $this->cardNotFound();
        }

        $encoded = base64_encode(json_encode([
            'type' => 'ean13',
            'text' => $options->value,
        ]));

        $url = config('app.ngrok') ? config('app.ngrok') . '/bot-api/barcode?data=' . $encoded : url('bot-api/barcode?data=' . $encoded);

        $attachment = json_decode($template->attachment, true);
        $attachment['attachment']['payload']['url'] = $url;

        return $attachment;
    }

    private function cardNotFound()
    {
        try {
            $story = StoryBot::findByKeyword('#cardnotfound', $this->subscriber->bot_id)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return ['text' => 'Not found stories with keyword #cardnotfound'];
        }

        $this->subscriber->branch = $story->id;
        $this->subscriber->save();

        return ['branch' => true];
    }
}
