<?php

namespace Modules\BarCode\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Modules\BarCode\Http\Requests\BarCodeRequest;
use Modules\BarCode\Lib\BarCodeService;

class BarCodeController extends Controller
{
    public function __invoke(BarCodeRequest $request)
    {
        $decoded = base64_decode($request->data);
        $data = json_decode($decoded, true);

        if (!$data) {
            return ['status' => 'error'];
        }

        $result = new BarCodeService();

        return $result->generate($data['type'] ?? 'code128', $data['text'] ?? 'Invalid data');
    }
}
