<?php

namespace Modules\BarCode\Lib;

use Response;
use Laminas\Barcode\Barcode;

class BarCodeService
{
    public function generate(string $type, string $text)
    {
        $barcode_options = [
            'text' => $text,
            'barHeight' => 70,
            'withBorder' => true,
            'factor' => 2
        ];

        if ($type === 'ean13') {
            $barcode_options['providedChecksum'] = true;
        }

        $renderer_options = [];
        $renderer = Barcode::factory(
            $type,
            'image',
            $barcode_options,
            $renderer_options
        );

        $file = $renderer->render();
        $response = Response::make($file, 200);

        $response->header("Content-Type", 'image/png');

        return $response;
    }
}
